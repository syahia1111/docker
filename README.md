# Setup 

create a workdirectory for the development, here you need to create a folder 

` mkdir odoo_training `

` cd odoo_training `

## 1- Create Odoo server 
Here we are using docker image, you need to make sure you have a basic docker info.

 1.1 Clone the Odoo addons, run this command:
 ``` git clone https://gitlab.com/eman.saeed/T-box ```

 1.2 Create a custom odoo.conf file

 ` touch odoo.conf `

 and edit the file with this content:

```
 [options]
addons_path = /T-box
data_dir = /var/lib/odoo
admin_passwd = 12345
limit_time_cpu = 1200
limit_time_real = 1800 
```


 1.3 setup docker-compose.yml file
    Run this comman:

     touch docker-compose.yml 

Edit the file with this content:

``` 
version: "3.7"
services:
  db:
    image: postgres:11
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=odoo
      - POSTGRES_PASSWORD=odoo
    volumes:
      - odoo15-db1-data:/var/lib/postgresql/data 
    
  odoo_web:
    image: odoo:15
   
    ports:
      - "8069:8069"
    volumes:
      - ./T-box:/T-box
      - ./odoo.conf:/etc/odoo/odoo.conf
      - odoo15-web-data:/var/lib/odoo
    #command:
      #-d db_name 
      #--update all
      

volumes:
  odoo15-web-data:
  odoo15-db1-data:


```

3- Run to install docker 

` sudo snap install docker `


4- Run the Odoo server 

` sudo docker-compose up `


